//async ----> 
//await ----> demora la operacion
const URL = "https://rickandmortyapi.com/api/character";
let characters = []; // contendra a todos los personajes globales de la aplicacion;
//funcion encargada de traer los personajes de la URL

const fetchCharacters = async (url=URL) => {
    //try -----> intenta esto 
    //catch ----> aino intenta esto otro si falla algo del try
    try {
        const response = await fetch(url);
        const { results: characters } = await response.json();
        //console.log(characters.info);
        return characters;
        
    } catch (err) {
        console.error(err)
    }
}
/*
    1- hacer una peticion HTTP a una url destino
*/

const showMessage = () => {
    // mostrar un mensaje en un span
    document.getElementById("message").innerHTML = "No hay personajes";
    document.getElementById("input").disable = true;
  };


const del = (id) => {
    document.getElementById(id).remove(); // método de document
    characters = characters.filter((character) => character.id != id); //
    console.log(characters);
    characters.length === 0 ? showMessage() : null;
  };
  

const createNode = ({id,image,name,species,gender,status}) => {
    const node = `
    <div class="col-md-4 col-12" id="${id}">
        <div class="card mt-5 ml-3">
         <img src="${image}">
            <div class="car-body">
                 <h5 class="card-title">${name}</h5>
                 <p class="card-text">Especie : ${species} - Sexo : ${gender}</p>
                 <p class="card-text"> ${status === "Alive" ? "Vivito y coleando 🧘‍♂️" : "Dead"}</p>
                 <button onclick="del(${id})" class="btn btn-block btn-danger">Borrar</ button>
             </div>
         </div>
    </div>
    `;
    document.getElementById("apiR").insertAdjacentHTML("beforeend", node);

}


//Funcion encargada de buscar un personaje por su nombre
function find () {
};
// funcion para cargar los personajes
const iterateCharacters = (characters) => {
    characters.map((character)=>createNode(character));
}

const searchCharacter = () => {
    const { value: name } = document.querySelector("#input");
    const foundCharacter = characters.find(
      (character) => character.name.toLowerCase() === name.toLowerCase()
    );
  
    console.log(foundCharacter);
  };

//carga el DOM
async function start () {
    document.getElementById("find").addEventListener("click",searchCharacter);
    characters = await fetchCharacters();
    iterateCharacters(characters);
    console.log(characters);
    

}
window.onload = start();



















